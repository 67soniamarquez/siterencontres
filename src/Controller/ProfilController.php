<?php

namespace App\Controller;

use App\Entity\Profil;
use App\Form\ProfilSearchType;
use App\Form\ProfilType;
use App\Repository\ProfilRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profil")
 */
class ProfilController extends AbstractController
{
    /**
     * @Route("/", name="profil_index")
     */
    public function index(ProfilRepository $profilRepository, Request $request): Response
    {
        $result = [];
        $resultCity = [];


        $profils = $profilRepository->findAll();
        $form = $this->createForm(ProfilSearchType::class);
        $form->handleRequest($request);

        //$age = $this->Profil->getAge();
        // dd($request->query->get('city'));

        if ($form->isSubmitted() && $form->isValid()) {

            // $dateNow = new \DateTime('now');
            $minAge = $form->get('minAge')->getData();
            $maxAge = $form->get('maxAge')->getData();
            $city = $form->get('city')->getData();
            //dd($city);
            if ($minAge == null  && $maxAge == null && $city == null) {

                $profils = $profilRepository->findAll();
            } else {

                foreach ($profils as $profil) {
                    //dd($dateNow->format('Y') - $profil->getDob()->format('Y'));
                    //      if ($dateNow - $profil->getDob())

                    $resultAge = $this->getAge($profil->getDob());
                    if ($resultAge >= $minAge && $resultAge <= $maxAge) {
                        $result[] .= $profil->getName();
                    }
                }
                $profils = $result;
                $profils = $profilRepository->findBy(['name' => $profils]);
                //dd($city->getId());
                foreach ($profils as $profil) {

                    if (($profil->getCity()->getId() == $city->getId())) {
                        $resultCity[] .= $profil->getName();
                        //dd($resultCity);
                    }
                }
                $profils = $resultCity;
                //dd($resultCity);
                $profils = $profilRepository->findBy(['name' => $profils]);
            }
            //dd($result);
        }

        return $this->render('profil/index.html.twig', [
            'profils' => $profils,
            //'age' => $profilRepository->findBy([$age]),
            'form' => $form->createView()
        ]);
    }

    public function getAge($date)
    {
        $referenceDate = date('01-01-Y');
        $referenceDateTimeObject = new \DateTime($referenceDate);
        $diff = $referenceDateTimeObject->diff($date);
        return $diff->y;
    }



    /**
     * @Route("/new", name="profil_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $profil = new Profil();
        $form = $this->createForm(ProfilType::class, $profil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $pictureFile = $form->get('picture')->getData();
            if ($pictureFile) {
                $fileName = md5(uniqid(rand())) . "." . $pictureFile->guessExtension();

                $fileDestination = $this->getParameter('profil_pictures_dir');

                try {
                    $pictureFile->move($fileDestination, $fileName);
                } catch (FileException $e) {
                    throw new HttpException(500, "An error occured during file upload");
                }
                $profil->setPicture($fileName);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($profil);
                $entityManager->flush();
            }

            return $this->redirectToRoute('profil_index');

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($profil);
            $entityManager->flush();

            return $this->redirectToRoute('profil_index');
        }

        return $this->render('profil/new.html.twig', [
            'profil' => $profil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="profil_show", methods={"GET"})
     */
    public function show(Profil $profil): Response
    {
        return $this->render('profil/show.html.twig', [
            'profil' => $profil,

        ]);
    }

    /**
     * @Route("/{id}/edit", name="profil_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Profil $profil): Response
    {
        $form = $this->createForm(ProfilType::class, $profil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profil_index');
        }

        return $this->render('profil/edit.html.twig', [
            'profil' => $profil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="profil_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Profil $profil): Response
    {
        if ($this->isCsrfTokenValid('delete' . $profil->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($profil);
            $entityManager->flush();
        }

        return $this->redirectToRoute('profil_index');
    }
}
