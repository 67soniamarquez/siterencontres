<?php

namespace App\Entity;

use App\Repository\ProfilRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProfilRepository::class)
 */
class Profil
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dob;

    public function getAge()
    {
        $dateInterval = $this->dob->diff(new \DateTime());
        return $dateInterval->y;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $picture;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="profils")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="profils")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Mes_preferences;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="profils")
     * @ORM\JoinColumn(nullable=false)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="profilsearch")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typesearch;

    /**
     * @var int|null
     */
    private $maxAge;


    /**
     *  @var int|null
     */
    private $minAge;

    /**
     * @return int|null
     */
    public function getMaxAge(): ?int
    {
        return $this->maxAge;
    }

    /**
     * @param int|null $maxAge
     * @return profilSearch
     */
    public function setMaxAge(int $maxAge): Profil
    {
        $this->maxAge = $maxAge;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinAge(): ?int
    {
        return $this->minAge;
    }

    /**
     * @param int|null $minAge
     * @return profilSearch
     */
    public function setMinAge(int $minAge): Profil
    {
        $this->minAge = $minAge;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    // public function __toString()
    // {
    //     return $this->name;
    // }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDob(): ?\DateTimeInterface
    {
        return $this->dob;
    }

    public function setDob(\DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->type;
    }

    public function setType(?Type $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getMesPreferences(): ?Category
    {
        return $this->Mes_preferences;
    }

    public function setMesPreferences(?Category $Mes_preferences): self
    {
        $this->Mes_preferences = $Mes_preferences;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getTypesearch(): ?Type
    {
        return $this->typesearch;
    }

    public function setTypesearch(?Type $typesearch): self
    {
        $this->typesearch = $typesearch;

        return $this;
    }
}
