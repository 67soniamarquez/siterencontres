<?php

namespace App\Entity;

use App\Repository\ProfilRepository;

class ProfilSearch
{

    /**
     * @var int|null
     */
    private $maxAge;


    /**
     *  @var int|null
     */
    private $minAge;

    /**
     *  @var Column(type="datetime")
     */
    private $dob;

    public function getAge()
    {
        $dateInterval = $this->dob->diff(new \DateTime());

        return $dateInterval->y;
    }

    public function getDob(): ?\DateTimeInterface
    {
        return $this->dob;
    }

    public function setDob(\DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     *  @Return int|null
     */
    public function getMaxAge(): ?int
    {
        $dateInterval = $this->dob->diff(new \DateTime());

        return $dateInterval->y;
        return $this->maxAge;
    }

    /**
     * @param int|null $maxAge
     * @return profilSearch
     */
    public function setMaxAge(int $maxAge): ProfilSearch
    {
        $this->maxAge = $maxAge;
        return $this;
    }

    /**
     *  @Return int|null
     */
    public function getMinAge(): ?int
    {
        return $this->minAge;
    }

    /**
     * @param int|null $minAge
     * @return profilSearch
     */
    public function setMinAge(int $minAge): ProfilSearch
    {
        $this->minAge = $minAge;
        return $this;
    }
}
