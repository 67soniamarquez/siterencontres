<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 */
class Type
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $genre;

    /**
     * @ORM\OneToMany(targetEntity=Profil::class, mappedBy="type")
     */
    private $profils;

    /**
     * @ORM\OneToMany(targetEntity=Profil::class, mappedBy="typesearch")
     */
    private $profilsearch;

    public function __construct()
    {
        $this->profils = new ArrayCollection();
        $this->profilsearch = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->genre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenre(): ?string
    {
        return $this->genre;
    }

    public function setGenre(string $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return Collection|Profil[]
     */
    public function getProfils(): Collection
    {
        return $this->profils;
    }

    public function addProfil(Profil $profil): self
    {
        if (!$this->profils->contains($profil)) {
            $this->profils[] = $profil;
            $profil->setType($this);
        }

        return $this;
    }

    public function removeProfil(Profil $profil): self
    {
        if ($this->profils->removeElement($profil)) {
            // set the owning side to null (unless already changed)
            if ($profil->getType() === $this) {
                $profil->setType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Profil[]
     */
    public function getProfilsearch(): Collection
    {
        return $this->profilsearch;
    }

    public function addProfilsearch(Profil $profilsearch): self
    {
        if (!$this->profilsearch->contains($profilsearch)) {
            $this->profilsearch[] = $profilsearch;
            $profilsearch->setTypesearch($this);
        }

        return $this;
    }

    public function removeProfilsearch(Profil $profilsearch): self
    {
        if ($this->profilsearch->removeElement($profilsearch)) {
            // set the owning side to null (unless already changed)
            if ($profilsearch->getTypesearch() === $this) {
                $profilsearch->setTypesearch(null);
            }
        }

        return $this;
    }
}
